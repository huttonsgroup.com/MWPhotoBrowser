#
# Be sure to run `pod lib lint MWPhotoBrowser2.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'MWPhotoBrowser2'
s.version          = '1.0.2'
s.summary          = 'An improvement of MWPhotoBrowser.'

s.description      = <<-DESC
MWPhotoBrowser can display one or more images or videos by providing either UIImage
objects, PHAsset objects, or URLs to library assets, web images/videos or local files.
The photo browser handles the downloading and caching of photos from the web seamlessly.
Photos can be zoomed and panned, and optional (customisable) captions can be displayed.
DESC

s.homepage         = 'https://gitlab.com/whitepixel.io/MWPhotoBrowser'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'Thien Huynh' => 'minhthien.huynh@whitepixel.io' }
s.source           = { :git => 'https://gitlab.com/whitepixel.io/MWPhotoBrowser.git', :tag => s.version.to_s }

s.ios.deployment_target = '8.0'

s.source_files = 'Pod/Classes/**/*'

s.resource_bundles = {
'MWPhotoBrowser' => ['Pod/Assets/*.png']
}
s.requires_arc = true

s.frameworks = 'ImageIO', 'QuartzCore', 'AssetsLibrary', 'MediaPlayer'
s.weak_frameworks = 'Photos'

s.dependency 'MBProgressHUD', '~> 0.9'
s.dependency 'DACircularProgress', '~> 2.3'

# SDWebImage
# 3.7.2 contains bugs downloading local files
# https://github.com/rs/SDWebImage/issues/1109
s.dependency 'SDWebImage', '~> 3.7', '!= 3.7.2'

end
