# MWPhotoBrowser2

[![Version](https://img.shields.io/cocoapods/v/MWPhotoBrowser2.svg?style=flat)](1.0.1)

## Installation

1. Run command:
pod repo add MWPhotoBrowser2 https://gitlab.com/whitepixel.io/MWPhotoBrowser

2. Add to Podfile:

```ruby
source 'https://gitlab.com/whitepixel.io/MWPhotoBrowser.git'
........
........
pod 'MWPhotoBrowser2'
```

## Author

Thien Huynh, minhthien.huynh@whitepixel.io

## License

MWPhotoBrowser2 is available under the MIT license. See the LICENSE file for more info.
